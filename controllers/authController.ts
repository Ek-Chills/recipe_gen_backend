import {Request, Response} from 'express'
import {randomUUID} from 'crypto'
import { db } from '../db'
import { activateToken, user } from '../db/schema'
import bcrypt from 'bcrypt'
import {sendMail} from '../auth'
import { getEmailVerificationBody } from '../auth/confirmationTemplate'
import { eq } from 'drizzle-orm'
import { encrypt } from '../auth/auth'

const registerController = async(req:Request, res:Response) => {
  console.log(req.body);
  
  try {
    const {email, password, verificationUrl}:{email:string; password:string; verificationUrl:string;} = req.body
    if(!email || !password || !verificationUrl) {
      return res.send('bad request').status(400)
    }
    const hashedPassword = await bcrypt.hash(password, 10)
    const randToken =  (randomUUID() + randomUUID()).replaceAll("-", "")
    const createdUser = await db.insert(user).values({email,password:hashedPassword}).returning()
    await db.insert(activateToken).values({token:randToken, userId:createdUser[0].id})
    await sendMail({
      body:getEmailVerificationBody(verificationUrl, randToken),
      subject:'Email verification',
      to:email
    })



    res.json({message:'user created successfully', redirectUrl:verificationUrl}).status(201)
  } catch (error) {
    console.log(error);
    res.json({error:error}).status(500)
    
  }

}

export const verifyEmailController = () => {
  try {
    
  } catch (error) {
    
  }
}

export const loginController = async(req:Request, res:Response) => {
  try {
    const {email, password}:{email:string; password:string;} = req.body
    if(!email || !password) return res.send('bad request').status(400)
    const dbUser = await db.query.user.findFirst({where:eq(user.email,email)})
    if(!dbUser) return res.status(404).send('user not found')
    if(!dbUser.active) return res.status(401).send('user account is not activated')
    const isPasswordValid = await bcrypt.compare(password, dbUser.password!)
    if(!isPasswordValid) return res.status(401).send('invalid credentials')
    const accessToken = await encrypt({email, password})
    return res.json({email:dbUser.email, accessToken, userId:dbUser.id})
  } catch (error) {
    console.log(error);
  }
}

export const activateAccountController = async(req:Request, res:Response) => {
  try {
    const {token}:{token:string} = req.body
  
    const activatedToken = await db.query.activateToken.findFirst({where:eq(activateToken.token,token)})
    if(!activatedToken) return res.send('invalid token').status(400)
    await db.update(activateToken).set({activatedAt:new Date(Date.now())}).where(eq(activateToken.token, token)).returning()
    await db.update(user).set({active:true}).where(eq(user.id, activatedToken.userId))
    return res.json({message:'account activated', success:true}).status(200)
  } catch (error) {
    console.log(error);
    
  }
  
  
}


export {registerController}