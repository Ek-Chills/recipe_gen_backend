import express, { Request, Response, Express} from 'express'
import {connectToDB} from './db'
import cors from 'cors'
import router  from './routers/authRouter';
import recipeRouter from './routers/recipeRouter'

const app:Express = express()
const port = 3000;

connectToDB()
app.use(cors())
app.use(express.json())


app.use('/api/auth', router)
app.use('/api/recipe', recipeRouter)

app.get('/', (req:Request, res:Response) => {
  res.json({message:'hello'})
})


app.listen(port, () => {
  console.log(`server is running on port ${port}`);  
})


