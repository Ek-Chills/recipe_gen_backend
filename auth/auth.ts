import * as jose from "jose";

const key = new TextEncoder().encode(process.env.JWT_SECRET!);

export const encrypt = async (payload: { email: string; password: string }) => {
  return await new jose.SignJWT(payload)
    .setProtectedHeader({ alg: "HS256" })
    .setIssuedAt()
    .setExpirationTime("1 day")
    .sign(key)
};


export async function decrypt(token: string) {
  const {payload} = await jose.jwtVerify(token, key, {algorithms:['HS256']} )
  return payload
}

