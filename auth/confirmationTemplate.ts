function getEmailVerificationBody(verificationLink: string, activateToken:string) {
  const template = `
    <html>
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Email Verification</title>
        <style>
          body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            color: #333333;
            padding: 20px;
          }
          .container {
            max-width: 600px;
            margin: 0 auto;
            background-color: #ffffff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
          }
          h1 {
            color: #333333;
            text-align: center;
          }
          p {
            line-height: 1.5;
          }
          .verification-link {
            text-align: center;
            margin-top: 20px;
          }
          .verification-link a {
            display: inline-block;
            padding: 10px 20px;
            background-color: #007bff;
            color: #ffffff;
            text-decoration: none;
            border-radius: 5px;
          }
          .verification-link a:hover {
            background-color: #0056b3;
          }
        </style>
      </head>
      <body>
        <div class="container">
          <h1>Email Verification</h1>
          <p>Dear User,</p>
          <p>Please click the following link to verify your email address:</p>
          <div class="verification-link">
            <a href="${verificationLink + '/' + activateToken}">Verify Email</a>
          </div>
          <p>This link is valid for a limited time only. Please click it as soon as possible.</p>
          <p>Thank you for your cooperation.</p>
          <p>Best regards,<br>Your App Team</p>
        </div>
      </body>
    </html>
  `;
  return template;
}

export {getEmailVerificationBody}