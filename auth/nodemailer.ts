import nodemailer from "nodemailer";
import * as dotenv from 'dotenv';

dotenv.config();


const sendMail = async({
  to,
  body,
  subject,
}: {
  to: string;
  subject: string;
  body: string;
}) => {
    const {SMTP_EMAIL,SMTP_PASSWORD} = process.env
    console.log('smtps are ', SMTP_EMAIL, SMTP_PASSWORD);
    

    const transport = nodemailer.createTransport({
        service:'gmail',
        auth:{
            user:SMTP_EMAIL,
            pass:SMTP_PASSWORD
        }
    })
    try {
        const testResult = await transport.verify()
        console.log(testResult);
        
    } catch (error) {
        console.log(error);
    }

    try {
       const sendResult = await transport.sendMail({
        from:SMTP_EMAIL,
        to,
        subject,
        html:body
       }) 
       console.log(sendResult);
       
    } catch (error) {
        console.log(error);
        
    }
}

export {sendMail}