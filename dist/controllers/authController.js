"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.registerController = exports.activateAccountController = exports.loginController = exports.verifyEmailController = void 0;
const crypto_1 = require("crypto");
const db_1 = require("../db");
const schema_1 = require("../db/schema");
const bcrypt_1 = __importDefault(require("bcrypt"));
const auth_1 = require("../auth");
const confirmationTemplate_1 = require("../auth/confirmationTemplate");
const drizzle_orm_1 = require("drizzle-orm");
const auth_2 = require("../auth/auth");
const registerController = async (req, res) => {
    console.log(req.body);
    try {
        const { email, password, verificationUrl } = req.body;
        if (!email || !password || !verificationUrl) {
            return res.send('bad request').status(400);
        }
        const hashedPassword = await bcrypt_1.default.hash(password, 10);
        const randToken = ((0, crypto_1.randomUUID)() + (0, crypto_1.randomUUID)()).replaceAll("-", "");
        const createdUser = await db_1.db.insert(schema_1.user).values({ email, password: hashedPassword }).returning();
        await db_1.db.insert(schema_1.activateToken).values({ token: randToken, userId: createdUser[0].id });
        await (0, auth_1.sendMail)({
            body: (0, confirmationTemplate_1.getEmailVerificationBody)(verificationUrl, randToken),
            subject: 'Email verification',
            to: email
        });
        res.json({ message: 'user created successfully', redirectUrl: verificationUrl }).status(201);
    }
    catch (error) {
        console.log(error);
        res.json({ error: error }).status(500);
    }
};
exports.registerController = registerController;
const verifyEmailController = () => {
    try {
    }
    catch (error) {
    }
};
exports.verifyEmailController = verifyEmailController;
const loginController = async (req, res) => {
    try {
        const { email, password } = req.body;
        if (!email || !password)
            return res.send('bad request').status(400);
        const dbUser = await db_1.db.query.user.findFirst({ where: (0, drizzle_orm_1.eq)(schema_1.user.email, email) });
        if (!dbUser)
            return res.status(404).send('user not found');
        if (!dbUser.active)
            return res.status(401).send('user account is not activated');
        const isPasswordValid = await bcrypt_1.default.compare(password, dbUser.password);
        if (!isPasswordValid)
            return res.status(401).send('invalid credentials');
        const accessToken = await (0, auth_2.encrypt)({ email, password });
        return res.json({ email: dbUser.email, accessToken, userId: dbUser.id });
    }
    catch (error) {
        console.log(error);
    }
};
exports.loginController = loginController;
const activateAccountController = async (req, res) => {
    try {
        const { token } = req.body;
        const activatedToken = await db_1.db.query.activateToken.findFirst({ where: (0, drizzle_orm_1.eq)(schema_1.activateToken.token, token) });
        if (!activatedToken)
            return res.send('invalid token').status(400);
        await db_1.db.update(schema_1.activateToken).set({ activatedAt: new Date(Date.now()) }).where((0, drizzle_orm_1.eq)(schema_1.activateToken.token, token)).returning();
        await db_1.db.update(schema_1.user).set({ active: true }).where((0, drizzle_orm_1.eq)(schema_1.user.id, activatedToken.userId));
        return res.json({ message: 'account activated', success: true }).status(200);
    }
    catch (error) {
        console.log(error);
    }
};
exports.activateAccountController = activateAccountController;
