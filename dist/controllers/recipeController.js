"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getRecipe = exports.deleteRecipe = exports.getRecipes = exports.saveRecipe = exports.generateRecipe = void 0;
const drizzle_orm_1 = require("drizzle-orm");
const index_1 = require("../db/index");
const schema_1 = require("../db/schema");
const openai_1 = __importDefault(require("openai"));
const openai = new openai_1.default({
    apiKey: process.env.OPENAI_SECRET_KEY, // Replace with your OpenAI API key
});
const generateRecipe = async (req, res) => {
    try {
        const { prompt } = req.body;
        const completion = await openai.chat.completions.create({
            messages: [
                {
                    role: "system",
                    content: `You are a helpful first class chef. your job is to answer the prompts of the user that contains the food he or she needs. \n You to generate the recipe for them make sure you only return the recipes and make sure its according to what the user inputted. \n either its a single food or a sentence make sure to decipher accordingly and only return the steps of the recipes. \n
            your answers must be in the form of array of objects that are stringified for my javascript backend to be able to parse it . your answer should also just have a short description of the food before the arrays of steps \n \n
            it should look like this 
            {"foodDescription":"some description about the food","steps":[{"stepNo":1, "step":"some step"},{"stepNo":2, "step":"some step"},]} make sure there are no curly braces that are not closed. Make sure you dont include line breaks or slashes either front or back slashes just give the response how you've been told so i can successfully run JSON.parse on it. i dont want it looking like \"foodDescription\":\"Egusi Soup is a popular Nigerian dish made with ground melon seeds and vegetables.\",\"steps\":[{\"stepNo\":1, \"step\":\"Heat palm oil in a pot and add chopped onions, garlic, and ginger. Saute until fragrant.\"},{\"stepNo\":2 `,
                },
                {
                    role: "user",
                    content: prompt,
                },
            ],
            model: "gpt-3.5-turbo",
            temperature: 0.5,
        });
        if (!completion) {
            throw new Error("OpenAI API error");
        }
        const parsableJson = completion?.choices[0].message.content?.replace(/\\/g, "");
        if (parsableJson) {
            const parsedResponse = JSON.parse(parsableJson);
            res.send({
                recipe: parsedResponse,
            });
        }
        else {
            res.status(500).json({
                error: "Invalid response from OpenAI API",
            });
        }
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({
            error: "An error occurred while generating the recipe",
        });
    }
};
exports.generateRecipe = generateRecipe;
const saveRecipe = async (req, res) => {
    const userId = req.params.id;
    const { description, name, steps, } = req.body;
    try {
        const savedRecipe = await index_1.db
            .insert(schema_1.recipe)
            .values({
            description,
            steps,
            userId: +userId,
            name,
        })
            .returning();
        return res.json(savedRecipe);
    }
    catch (error) {
        console.log(error);
        res.status(500).send("internal server error");
    }
    console.log(userId);
    res.send(userId);
};
exports.saveRecipe = saveRecipe;
const getRecipes = async (req, res) => {
    const userId = req.params.id;
    try {
        const recipes = await index_1.db
            .select()
            .from(schema_1.recipe)
            .where((0, drizzle_orm_1.eq)(schema_1.recipe.userId, +userId));
        if (schema_1.recipe) {
            return res.json(recipes);
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).send("internal server error");
    }
};
exports.getRecipes = getRecipes;
const deleteRecipe = async (req, res) => {
    const recipeId = req.params.id;
    try {
        const deletedRecipe = await index_1.db
            .delete(schema_1.recipe)
            .where((0, drizzle_orm_1.eq)(schema_1.recipe.id, +recipeId));
        if (deletedRecipe) {
            return res.json(deletedRecipe);
        }
    }
    catch (err) {
        console.log(err);
        res.status(500).send("internal server error");
    }
};
exports.deleteRecipe = deleteRecipe;
const getRecipe = async (req, res) => {
    console.log('hit id', req.params.id);
    const recipeId = req.params.id;
    try {
        const dbRecipe = await index_1.db.query.recipe.findFirst({
            where: (0, drizzle_orm_1.eq)(schema_1.recipe.id, +recipeId)
        });
        if (dbRecipe) {
            return res.json(dbRecipe);
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).send("internal server error");
    }
};
exports.getRecipe = getRecipe;
