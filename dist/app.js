"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const db_1 = require("./db");
const cors_1 = __importDefault(require("cors"));
const authRouter_1 = __importDefault(require("./routers/authRouter"));
const recipeRouter_1 = __importDefault(require("./routers/recipeRouter"));
const app = (0, express_1.default)();
const port = 3000;
(0, db_1.connectToDB)();
app.use((0, cors_1.default)());
app.use(express_1.default.json());
app.use('/api/auth', authRouter_1.default);
app.use('/api/recipe', recipeRouter_1.default);
app.get('/', (req, res) => {
    res.json({ message: 'hello' });
});
app.listen(port, () => {
    console.log(`server is running on port ${port}`);
});
