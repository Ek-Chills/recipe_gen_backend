"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const recipeController_1 = require("../controllers/recipeController");
const express_1 = __importDefault(require("express"));
const router = express_1.default.Router();
router.post('/createRecipe', recipeController_1.generateRecipe);
router.post('/save/:id', recipeController_1.saveRecipe);
router.get('/getRecipe/:id', recipeController_1.getRecipes);
router.get('/getSingleRecipe/:id', recipeController_1.getRecipe);
router.delete('/deleteRecipe/:id', recipeController_1.deleteRecipe);
exports.default = router;
