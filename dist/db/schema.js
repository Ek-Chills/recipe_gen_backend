"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.userRelation = exports.recipeRelations = exports.activateTokenRelation = exports.recipe = exports.activateToken = exports.user = exports.pgTable = void 0;
const drizzle_orm_1 = require("drizzle-orm");
const pg_core_1 = require("drizzle-orm/pg-core");
exports.pgTable = (0, pg_core_1.pgTableCreator)((name) => `recipe_${name}`);
exports.user = (0, exports.pgTable)('user', {
    id: (0, pg_core_1.serial)('id').notNull().primaryKey(),
    email: (0, pg_core_1.text)('email'),
    emailVerified: (0, pg_core_1.timestamp)('emailVerified', { mode: 'date' }),
    active: (0, pg_core_1.boolean)('active'),
    password: (0, pg_core_1.text)('password'),
    createdAt: (0, pg_core_1.timestamp)('createdAt').defaultNow(),
});
exports.activateToken = (0, exports.pgTable)('activateToken', {
    id: (0, pg_core_1.serial)('id').primaryKey(),
    token: (0, pg_core_1.text)('token').unique(),
    activatedAt: (0, pg_core_1.timestamp)('activatedAt'),
    createdAt: (0, pg_core_1.timestamp)('createdAt').defaultNow(),
    userId: (0, pg_core_1.serial)('userId')
});
exports.recipe = (0, exports.pgTable)('recipe', {
    id: (0, pg_core_1.serial)('id').notNull().primaryKey(),
    name: (0, pg_core_1.text)('name'),
    description: (0, pg_core_1.text)('description'),
    steps: (0, pg_core_1.jsonb)('steps'),
    createdAt: (0, pg_core_1.timestamp)('createdAt').defaultNow(),
    userId: (0, pg_core_1.serial)('userId')
});
exports.activateTokenRelation = (0, drizzle_orm_1.relations)(exports.activateToken, ({ one }) => ({
    user: one(exports.user, {
        fields: [exports.activateToken.userId],
        references: [exports.user.id],
        relationName: 'activateTokenRelation'
    })
}));
exports.recipeRelations = (0, drizzle_orm_1.relations)(exports.recipe, ({ one }) => ({
    user: one(exports.user, {
        fields: [exports.recipe.userId],
        references: [exports.user.id],
        relationName: 'recipeRelations'
    })
}));
exports.userRelation = (0, drizzle_orm_1.relations)(exports.user, ({ many }) => ({
    activateToken: many(exports.activateToken),
    recipe: many(exports.recipe)
}));
