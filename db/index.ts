import { drizzle } from "drizzle-orm/node-postgres";
import { pgTableCreator } from "drizzle-orm/pg-core";
import { Client } from "pg";
import * as schema from "./schema";
const client = new Client({
  connectionString: "postgres://postgres.lwktgvrweqncnlidinvy:Ekene13881609@aws-0-eu-west-2.pooler.supabase.com:5432/postgres",
});

export const connectToDB = async() => {
  try {
    await client.connect();
    console.log('connected successfully');
    
  } catch (error) {
    console.log(error);
  }

}
export const db = drizzle(client,{schema});
export const pgTable = pgTableCreator((name) => `recipe_${name}`);