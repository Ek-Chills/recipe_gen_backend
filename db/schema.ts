import { relations } from "drizzle-orm";
import { boolean, jsonb, pgTableCreator, serial, text, timestamp} from "drizzle-orm/pg-core";
export const pgTable = pgTableCreator((name) => `recipe_${name}`);

export const user = pgTable('user', {
  id:serial('id').notNull().primaryKey(),
  email:text('email'),
  emailVerified:timestamp('emailVerified', {mode:'date'}),
  active:boolean('active'),
  password:text('password'),
  createdAt:timestamp('createdAt').defaultNow(),
})

export const activateToken = pgTable('activateToken', {
  id:serial('id').primaryKey(),
  token:text('token').unique(),
  activatedAt:timestamp('activatedAt'),
  createdAt:timestamp('createdAt').defaultNow(),
  userId:serial('userId')
})

export const recipe = pgTable('recipe', {
  id:serial('id').notNull().primaryKey(),
  name:text('name'),
  description:text('description'),
  steps:jsonb('steps'),
  createdAt:timestamp('createdAt').defaultNow(),
  userId:serial('userId')
})

export const activateTokenRelation = relations(activateToken, ({one}) => ({
  user: one(user, {
    fields:[activateToken.userId],
    references:[user.id],
    relationName:'activateTokenRelation'
  })
}))


export const recipeRelations = relations(recipe, ({one}) => ({
  user: one(user, {
    fields:[recipe.userId],
    references:[user.id],
    relationName:'recipeRelations'
  })
}))

export const userRelation = relations(user, ({ many}) => ({
  activateToken:many(activateToken),
  recipe:many(recipe)
}))

