import { deleteRecipe, generateRecipe, getRecipe, getRecipes, saveRecipe } from '../controllers/recipeController'
import express from 'express'

const router = express.Router()


router.post('/createRecipe', generateRecipe)
router.post('/save/:id', saveRecipe)
router.get('/getRecipe/:id', getRecipes)
router.get('/getSingleRecipe/:id', getRecipe)
router.delete('/deleteRecipe/:id', deleteRecipe)

export default router