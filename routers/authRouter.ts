import express from 'express'
import {activateAccountController, loginController, registerController} from '../controllers/authController'

const router = express.Router()


// const login = (req:Request, res:Response, next:NextFunction) => {

// }

router.post('/register', registerController)
router.post('/login', loginController)
router.post('/activate', activateAccountController)


export default router